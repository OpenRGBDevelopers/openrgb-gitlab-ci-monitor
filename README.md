# OpenRGB icons

Custom font for OpenRGB and plugins usage.

## Download

![download ttf](https://gitlab.com/OpenRGBDevelopers/openrgb-icons/-/jobs/artifacts/main/download?job=generate-font)
