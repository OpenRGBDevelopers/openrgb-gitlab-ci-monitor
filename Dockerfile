FROM python:bullseye

RUN \
 apt update -y && \ 
 apt install -y fontforge && \
 wget https://raw.githubusercontent.com/pteromys/svgs2ttf/master/svgs2ttf && \
 install svgs2ttf /usr/bin/svgs2ttf
  
